import Vue from 'vue'
import Router from 'vue-router'
import ExecFile from '@/components/ExecFile/ExecFile'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'exec-file',
      component: ExecFile
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
