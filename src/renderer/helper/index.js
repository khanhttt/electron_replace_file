/* eslint-disable no-tabs */
const fs = require('fs')
export default {
  getCurrentTime: function () {
    const today = new Date()
    const date =
			today.getDate() +
			'-' +
			(today.getMonth() + 1) +
			'-' +
			today.getFullYear()
    return date
  },
  readFile: function (filepath) {
    return new Promise((resolve, reject) => {
      fs.readFile(filepath, 'utf8', (err, data) => {
        if (err) reject(err)
        else resolve(data)
      })
    })
  },
  createFolder () {
    const dir = __static + '/' + this.getCurrentTime()
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir)
    }
    return dir
  },
  saveFile (filepath, content) {
    return new Promise((resolve, reject) => {
      fs.writeFile(filepath, content, err => {
        if (err) reject(err)
        else resolve()
      })
    })
  }
}
